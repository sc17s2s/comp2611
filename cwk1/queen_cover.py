## Definition of Queen Cover search problem

from __future__ import print_function
from copy import deepcopy

# Representation of a state: (full_board_state)
# 
# So on a small 3x3 board after a queen has been placed we might have a state such as:
# ([[1,2,2],
#   [2,2,0],
#   [2,0,2]] )
# Where:
# 0 is empty
# 1 is a queen
# 2 is covered by queen

# Initialises the board state with 0 in each square
def queen_get_initial_state(x,y):
    return (matrix_of_zeros(y,x))

def matrix_of_zeros(X,Y):
    return [[0 for x in range(X)] for y in range(Y)]

# Possible positions to place a queen on the board
def queen_possible_actions(state):
    ans = []		# List of tuples to return
    for i in range(BOARD_X):
        for j in range(BOARD_Y):
            if state[i][j] == 0 or state[i][j] == 2:	# If the entry in the ith row and jth col is not a queen (1),
                ans.append((i,j))  						# Add the tuple (i,j) to the answer
    return ans

# Get the positions with no queen and place a queen
def queen_successor_state(action, state):
	board = deepcopy(state)
	xpos = action[0]
	ypos = action[1]
	for i in range(BOARD_X):
		for j in range(BOARD_Y):			# Change the entries for the squares covered by the queen to 2
			if i == action[0] and board[i][j] != 1:
				board[i][j] = 2
			if j == action[1] and board[i][j] != 1:
				board[i][j] = 2
			if abs(i-xpos) == abs(j-ypos) and board[i][j] != 1:
				board[i][j] = 2
	board[action[0]][action[1]] = 1
	
	return board

# Check if all positions have a queen or are covered by a queen
def queen_goal_state(state):
    board = state
    for row in board:
        for square in row:
			if square == 0:
				return False
				
    print_board_state(board)
    return True

# Print the board
def print_board_state(state):       
    board = state
    print("\nBOARD:")
    for row in board:
        for square in row:
            print( " %2i" % square, end = '' )
        print()

def queen_print_problem_info():
    print("The Queen Cover (", BOARD_X, "x", BOARD_Y, "board)")

# Return a problem spec tuple for a given board size
def make_qc_problem(x, y):
      global BOARD_X, BOARD_Y, queen_initial_state
      BOARD_X = x
      BOARD_Y = y
      queen_initial_state = queen_get_initial_state(x,y)
      return  ( None,
                queen_print_problem_info,
                queen_initial_state,
                queen_possible_actions,
                queen_successor_state,
                queen_goal_state
              )
